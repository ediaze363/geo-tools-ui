import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RockSampleComponent } from './rock-sample.component';

describe('RockSampleComponent', () => {
  let component: RockSampleComponent;
  let fixture: ComponentFixture<RockSampleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RockSampleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RockSampleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
