import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RockSampleComponent } from './rock-sample/rock-sample.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { ApplicationSettingsComponent } from './application-settings/application-settings.component';

const routes: Routes = [
  { path: 'rock-sample', component: RockSampleComponent },
  { path: 'app-settings', component: ApplicationSettingsComponent }
];

@NgModule({
  declarations: [
    RockSampleComponent,
    ApplicationSettingsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
  exports: [
      RouterModule
  ]
})
export class AdminModule { }
