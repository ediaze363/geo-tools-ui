import { LayoutComponent } from '../layout/layout.component';

export const routes = [

    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: 'home', loadChildren: () => import('./home/home.module').then(m => m.HomeModule) },
            { path: 'admin', loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule) }
        ]
    },

    // Not found
    { path: '**', redirectTo: 'home' }

];
