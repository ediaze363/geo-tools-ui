
const Home = {
    text: 'Home',
    link: '/home',
    icon: 'icon-home'
};

const headingMain = {
    text: 'Main Navigation',
    heading: true
};

const headingComponents = {
  text: 'Modules',
  heading: true
};

const Admin = {
  text: 'Admin',
  link: '/admin',
  icon: 'icon-settings',
  submenu: [
    {
        text: 'Rock Sample',
        link: '/admin/rock-sample'
    },
    {
        text: 'Settings',
        link: '/admin/app-settings'
    }
  ]
};

export const menu = [
    headingMain,
    Home,
    headingComponents,
    Admin
];
