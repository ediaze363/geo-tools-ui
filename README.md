# geo-tools-ui

Geologist Dashboard control

# Angular Cli commands

## Uninstall and install angular cli

```
npm uninstall -g @angular/cli
npm cache clean --force
npm install -g @angular/cli
ng --version
```

## Update local angular cli

```
ng --version
npm uninstall --save-dev angular-cli
npm install --save-dev @angular/cli@latest
ng --version
```

## fix local issues

```
npm audit
npm audit fix
```

# Adding Features to Your Angular Application

You can use the ng generate command to add features to your existing application:

- `ng generate class my-new-class`: add a class to your application
- `ng generate component my-new-component`: add a component to your application
- `ng generate directive my-new-directive`: add a directive to your application
- `ng generate enum my-new-enum`: add an enum to your application
- `ng generate module my-new-module`: add a module to your application
- `ng generate pipe my-new-pipe`: add a pipe to your application
- `ng generate service my-new-service`: add a service to your application

The generate command and the different sub-commands also have shortcut notations, so the following commands are similar:

- `ng g cl my-new-class`: add a class to your application
- `ng g c my-new-component`: add a component to your application
- `ng g d my-new-directive`: add a directive to your application
- `ng g e my-new-enum`: add an enum to your application
- `ng g m my-new-module`: add a module to your application
- `ng g p my-new-pipe`: add a pipe to your application
- `ng g s my-new-service`: add a service to your application.

# References

- https://onlineconvertfree.com/convert-format/png-to-svg
- https://help.github.com/es/github/writing-on-github/basic-writing-and-formatting-syntax
- https://coreui.io/v1/demo/AngularJS_Demo/#!/icons/simple-line-icons
- https://www.sitepoint.com/ultimate-angular-cli-reference/

# Steps

```
cd src/app/routes
ng g m admin
cd admin
```